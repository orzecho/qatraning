package ge.com;

import ge.com.base.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class BaseTest {

    protected WebDriver driver;

    @BeforeTest(alwaysRun = true)
    @Parameters({"browser"})
    protected void setUpDriver(String browser){
        driver = DriverFactory.getDriver(browser);
    }

    @AfterTest(alwaysRun = true)
    protected void closeBrowser(){
        driver.quit();
    }



}
