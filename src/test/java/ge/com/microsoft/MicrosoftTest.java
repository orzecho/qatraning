package ge.com.microsoft;

import ge.com.BaseTest;
import ge.com.pages.GooglePages;
import ge.com.pages.GoogleResultPages;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class MicrosoftTest extends BaseTest {

    @Test
    public void microsoftTest() throws InterruptedException {
            driver.get("https://www.google.com");

//            Thread.sleep(3000);

            GooglePages googlePages = new GooglePages(driver);
            googlePages.globalSearch("microsoft");

            GoogleResultPages resultPages = new GoogleResultPages(driver);
            resultPages.changeToEnglish();

            WebElement resultBlocks = resultPages.getResultBlocks("Microsoft - Official Home Page");
            Assertions.assertThat(resultBlocks).isNotNull();
            Assertions.assertThat(resultPages.hasSearchBox(resultBlocks)).isTrue();

            resultPages.setSubSearch(resultBlocks, "surface");

            String globalSearchText = googlePages.readGlobalSearchText();
            Assertions.assertThat(globalSearchText).isEqualTo("surface site:microsoft.com");
    }
}
