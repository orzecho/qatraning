package ge.com.scrum;

import ge.com.BaseTest;
import ge.com.pages.ScrumMainPages;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ScrumCertificateTest extends BaseTest {

    @Test
    public void validateCertifications() throws InterruptedException {
//        Thread.sleep(3000);

        driver.get("https://www.scrum.org/certification-list");

        ScrumMainPages scrumMainPages = new ScrumMainPages(driver);
        scrumMainPages.closePopUpPolicyTerms();
        scrumMainPages.setFirstName("Rafał");
        scrumMainPages.setLastName("Markowicz");
        scrumMainPages.selectCertificate("certification");
        scrumMainPages.clickSearchButton();

        List<String> certificates = scrumMainPages.readCertificateList();

        assertThat(certificates).doesNotContain("PSU I");
    }
}
