package ge.com.base;

import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    protected WebDriver driver;

    protected BasePage(WebDriver driver){
        this.driver = driver;
    }

    protected void click(By locator){
        findElement(locator).click();
    }


    protected WebElement findElement(final By locator) {

        WebDriverWait wait = new WebDriverWait(driver, 15);
        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver webDriver) {
                return driver.findElement(locator);
            }
        });
        return element;
    }

    public void typeInputText (By locator, String text) {
        WebElement inputField = findElement(locator);
        inputField.click();
        inputField.clear();
        inputField.sendKeys(text);
    }

    public void typeInputTextWithSubmit(By locator, String text){
        WebElement inputField = findElement(locator);
        inputField.click();
        inputField.clear();
        inputField.sendKeys(text);
        inputField.submit();
    }

    public String getValueText(By locator){
        WebElement outPutField = findElement(locator);
        return outPutField.getAttribute("value");
    }

    public void typeInputTextWithSubmit(WebElement inputField, String text){
        inputField.clear();
        inputField.sendKeys(text);
        inputField.submit();
    }

    public void selectFromDropDown(By locator, String value){
        Select select = new Select(findElement(locator));
        select.selectByVisibleText(value);
    }

    public <T> void waitFor(ExpectedCondition<T> expectedCondition, Integer seconds){
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(expectedCondition);
    }

    public boolean existsElement(WebElement webElement, By nqsbq) {
        try {
            webElement.findElement(nqsbq);
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }


}


