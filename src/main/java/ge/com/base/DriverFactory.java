package ge.com.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static java.util.Objects.nonNull;

public class DriverFactory {

    public static WebDriver getDriver(String browser) {

        WebDriver driver = null;
        String pathToDriver = loadProperties().getProperty(browser);

        switch(browser) {
            case "chrome" :
                File chromeDriver = new File(pathToDriver);
                System.setProperty("webdriver.chrome.driver", chromeDriver.getAbsolutePath());
                driver = new ChromeDriver();
                break;
            case "firefox" :
                File geckoDriver = new File(pathToDriver);
                System.setProperty("webdriver.gecko.driver", geckoDriver.getAbsolutePath());
                driver = new FirefoxDriver();
                break;

        }

        driver.manage().window().maximize();
        return driver;

    }

    private static Properties loadProperties(){
        Properties properties = new Properties();
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream("src/main/resources/prop.properties");
            properties.load(inputStream);
            int i = 1+1;
        } catch (IOException ex) {
            ex.printStackTrace();
        }finally {
            if (nonNull(inputStream)) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return properties;
    }
}
