package ge.com.selectors;

import org.openqa.selenium.By;

public class GoogleResultPages {

    public static final By CHANGE_TO_ENGLISH = By.linkText("Change to English");
    public static final By GOOGLE_RESULTS_GROUP =  By.xpath("//*[@class='bkWMgd']");
    public static final By GOOGLE_SUBSEARCH = By.id("nqsbq");
    public static final By GOOGLE_SUBSEARCH_BUTTON = By.xpath("//*[@name='btnGNS']");


}
