package ge.com.selectors;

import org.openqa.selenium.By;

public class ScrumPageSelectors {

    public static final By PRIVACY_POLICY_CLOSE = By.className("privacy-dismiss");

    public static final By FIRST_NAME = By.xpath("//*[@id='edit-first-name']");
    public static final By LAST_NAME = By.xpath("//*[@id='edit-last-name']");
    public static final By SEARCH_BUTTON = By.xpath("//*[@id='edit-search']");
    public static final By DROP_BOX = By.xpath("//*[@id='edit-certification-name']");
    public static final By RESPONSIVE_TABLE = By.xpath(
            "//div[@class='responsive-table']//div[contains(@class, 'responsive-table-row') and not (contains(@class, 'responsive-table-headings'))]//div[3]");
    public static final By RESPONSIVE_TABLE_TEST = By.xpath("//div[@class='search-results-inner']");

}
