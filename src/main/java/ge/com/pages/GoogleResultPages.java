package ge.com.pages;

import ge.com.base.BasePage;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ge.com.selectors.GoogleResultPages.*;

public class GoogleResultPages extends BasePage
{
    public GoogleResultPages(WebDriver driver) {
        super(driver);
    }

    public void changeToEnglish(){click(CHANGE_TO_ENGLISH); }

    public WebElement getResultBlocks(String containString){
        List<WebElement> elements2 = driver.findElements(GOOGLE_RESULTS_GROUP);
        Optional<WebElement> any = elements2.stream().filter(x -> x.getText().contains(containString))
                .findAny();

        Assertions.assertThat(any.isPresent()).isTrue();
        return  any.get();
    }

    public boolean hasSearchBox(WebElement resultBlocks) {
        return existsElement(resultBlocks, GOOGLE_SUBSEARCH);
    }

    public void setSubSearch(WebElement webElement, String searchString){
        WebElement element = webElement.findElement(GOOGLE_SUBSEARCH);
        typeInputTextWithSubmit(element, searchString);
        click(GOOGLE_SUBSEARCH_BUTTON);

    }
}
