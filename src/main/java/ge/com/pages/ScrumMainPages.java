package ge.com.pages;

import ge.com.base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.stream.Collectors;

import static ge.com.selectors.ScrumPageSelectors.*;

public class ScrumMainPages extends BasePage {

    public ScrumMainPages(WebDriver driver){
        super(driver);
    }

    public void setFirstName(String firstName){
        typeInputText(FIRST_NAME, firstName);
    }

    public void setLastName(String lastName){
        typeInputText(LAST_NAME, lastName);
    }

    public void closePopUpPolicyTerms(){
        click(PRIVACY_POLICY_CLOSE);
    }

    public void selectCertificate(String certificate){
        selectFromDropDown(DROP_BOX, certificate);
    }

    public void clickSearchButton(){
        click(SEARCH_BUTTON);
    }

    public List<String> readCertificateList(){

        waitFor(ExpectedConditions.visibilityOfElementLocated(RESPONSIVE_TABLE_TEST), 5);
        return driver.findElements(RESPONSIVE_TABLE)
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }
}
