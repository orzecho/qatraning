package ge.com.pages;

import ge.com.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static ge.com.selectors.GooglePagesSelectors.SEARCH_FIELD;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

public class GooglePages extends BasePage {

    public GooglePages(WebDriver driver) {
        super(driver);
    }

    public void globalSearch(String key) {
        typeInputTextWithSubmit(SEARCH_FIELD, key);
        waitFor(presenceOfElementLocated(By.id("resultStats")),5);
    }

    public String readGlobalSearchText(){
        return getValueText(SEARCH_FIELD);
    }

}
